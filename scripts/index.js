"use strict";

window.onload = function () {
    initiateDropdownFootballTeams();
    const submitBtn = document.getElementById("submitBtn");
    submitBtn.onclick = onSubmitBtnClicked;
    document.getElementById("teamSelect").onchange = onChangeTeam;
}

function initiateDropdownFootballTeams() {
    let teams = [
        { code: "DAL", name: "Dallas Cowboys", plays: "Arlington, TX" },
        { code: "DEN", name: "Denver Broncos", plays: "Denver, CO" },
        { code: "HOU", name: "Houston Texans", plays: "Houston, TX" },
        { code: "KAN", name: "Kansas City Chiefs", plays: "Kansas City, MO" }
    ];
    const teamSelect = document.getElementById("teamSelect");

    teamSelect.appendChild(new Option("Select a team", ""));
    for (let i = 0; i < teams.length; i++) {
        teamSelect.appendChild(new Option(teams[i].name, teams[i].code));
    }
}

function onSubmitBtnClicked() {
    let teams = [
        { code: "DAL", name: "Dallas Cowboys", plays: "Arlington, TX" },
        { code: "DEN", name: "Denver Broncos", plays: "Denver, CO" },
        { code: "HOU", name: "Houston Texans", plays: "Houston, TX" },
        { code: "KAN", name: "Kansas City Chiefs", plays: "Kansas City, MO" }
    ];
    const teamSelect = document.getElementById("teamSelect");
    const para = document.getElementById("para");
    if (teamSelect.selectedIndex > 0) {
        para.innerHTML = "You selected the " + teamSelect.options[teamSelect.selectedIndex].text + " \(" + teamSelect.options[teamSelect.selectedIndex].value + "\)" + " who play in " + teams[teamSelect.selectedIndex - 1].plays;
    }
    else {
        alert('Please select an option');
    }
    return false;
}

function onChangeTeam() {
    const teamSelect = document.getElementById("teamSelect");
    const para = document.getElementById("para");

    if (teamSelect.value == "") {
        para.innerHTML = "";
        document.getElementById("myImage").alt = "";
        document.getElementById("myImage").src = "";           
    }
    else {
        if (teamSelect.value == "DAL") {
            document.getElementById("myImage").src = "images/dallas.webp";
            document.getElementById("myImage").alt = "Dallas logo";
        }
        else if (teamSelect.value == "DEN"){
            document.getElementById("myImage").alt = "Denver logo";
            document.getElementById("myImage").src = "images/denver.webp";
        }
        else if (teamSelect.value == "HOU") {
            document.getElementById("myImage").alt = "Texas logo";
            document.getElementById("myImage").src = "images/texas.webp";
        }
        else if (teamSelect.value == "KAN") {
            document.getElementById("myImage").alt = "Kansas logo";
            document.getElementById("myImage").src = "images/kansas.webp";
        }

    }
    return false;
}